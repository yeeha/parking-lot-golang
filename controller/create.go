package controller

import (
	"GOJEK/common"
	"GOJEK/service"
	"errors"
	"fmt"
	"strconv"
)

func create(args []string) string {
	total, err := strconv.Atoi(args[1])
	if err != nil {
		return errors.New("param must be integer").Error()
	}
	service.CreateParkingLot(total)
	return fmt.Sprintf("Created a parking lot with %d slots", total)
}

//Create is used as input output handler for create_parking_lot service call
func Create() string {
	var args []string
	args, err := common.GetArgs(1)
	if err != nil {
		return err.Error()
	}
	return create(args)
}
