package controller

import (
	"GOJEK/common"
	"GOJEK/service"
	"errors"
	"fmt"
	"strconv"
)

func leave(args []string) string {
	i, err := strconv.Atoi(args[1])
	if err != nil {
		return errors.New("param must be integer").Error()
	}
	err = service.Leave(i)
	if err != nil {
		return err.Error()
	}
	return fmt.Sprintf("Slot number %d is free", i)
}

//LeaveParkingLot is used as input output handler for leave service call
func LeaveParkingLot() string {
	var args []string
	args, err := common.GetArgs(1)
	if err != nil {
		return err.Error()
	}
	return leave(args)
}
