package controller

import (
	"GOJEK/common"
	"GOJEK/service"
)

//Status is used as input output handler for status service call
func Status() string {
	_, err := common.GetArgs(0)
	if err != nil {
		return err.Error()
	}
	return service.Status()
}
