package controller

import (
	"GOJEK/model"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreate(t *testing.T) {
	var args []string
	args = append(args, "")
	args = append(args, "6")
	assert.Equal(t, "Created a parking lot with 6 slots", create(args), "test creating 6 slots of parking lot")
	args[1] = "not integer"
	assert.Equal(t, "param must be integer", create(args), "test create_parking_lot param must be integer")
}

func TestPark(t *testing.T) {

	var args []string
	args = append(args, "")
	args = append(args, "4")
	assert.Equal(t, "No car on parking lot", leave(args), "test leave when no car on parking lot")

	args[1] = "KA-01-HH-1234"
	args = append(args, "White")
	assert.Equal(t, "Allocated slot number: 1", park(args), "test #1 run parking")
	args[1] = "KA-01-HH-9999"
	args[2] = "White"
	assert.Equal(t, "Allocated slot number: 2", park(args), "test #2 run parking")
	args[1] = "KA-01-BB-0001"
	args[2] = "Black"
	assert.Equal(t, "Allocated slot number: 3", park(args), "test #3 run parking")
	args[1] = "KA-01-HH-7777"
	args[2] = "Red"
	assert.Equal(t, "Allocated slot number: 4", park(args), "test #4 run parking")
	args[1] = "KA-01-HH-2701"
	args[2] = "Blue"
	assert.Equal(t, "Allocated slot number: 5", park(args), "test #5 run parking")
	args[1] = "KA-01-HH-3141"
	args[2] = "Black"
	assert.Equal(t, "Allocated slot number: 6", park(args), "test #6 run parking")
	args[1] = "FULL"
	args[2] = "FULL"
	assert.Equal(t, "Sorry, parking lot is full", park(args), "test park when parking lot is full")
}

func TestLeave(t *testing.T) {
	var args []string
	args = append(args, "")
	args = append(args, "4")
	assert.Equal(t, "Slot number 4 is free", leave(args), "test leave car on slot 4")
	args[1] = "NOT INTEGER"
	assert.Equal(t, "param must be integer", leave(args), "test leave argument should integer")
}

func TestParkAgain(t *testing.T) {
	var args []string
	args = append(args, "")
	args = append(args, "KA-01-P-333")
	args = append(args, "White")
	assert.Equal(t, "Allocated slot number: 4", park(args), "test park car after car leave")
	args[1] = "DL-12-AA-9999"
	args[2] = "White"
	assert.Equal(t, "Sorry, parking lot is full", park(args), "test park when parking lot is full")
}

func TestFind(t *testing.T) {
	var args []string
	args = append(args, "")
	args = append(args, "White")
	assert.Equal(t, "KA-01-HH-1234, KA-01-HH-9999, KA-01-P-333", find(RegistrationNumbersForCarsWithColour, args), "find registration number by color (results an array)")

	args[1] = "White"
	assert.Equal(t, "1, 2, 4", find(SlotNumbersForCarsWithColour, args), "test find slots number by car colour (results an array)")

	args[1] = "KA-01-HH-3141"
	assert.Equal(t, "6", find(SlotNumberForRegistrationNumber, args), "test find car by registration number (single value result)")

	args[1] = "MH-04-AY-1111"
	assert.Equal(t, "Not found", find(SlotNumberForRegistrationNumber, args), "test find car by registration number (single value result)")

	args[1] = ""
	assert.Equal(t, "Search Criteria Empty", find(SlotNumbersForCarsWithColour, args), "test find slot number by car colour when search criteria empty")

	args[1] = ""
	assert.Equal(t, "Search Criteria Empty", find(RegistrationNumbersForCarsWithColour, args), "test find registration number by car colour when search criteria empty")

	args[1] = ""
	assert.Equal(t, "Search Criteria Empty", find(SlotNumberForRegistrationNumber, args), "test find slot number by registration number when search criteria empty")

	os.Remove(model.Datasource)
}
