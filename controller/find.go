package controller

import (
	"GOJEK/common"
	"GOJEK/model"
	"GOJEK/service"
	"bytes"
	"fmt"
)

//RegistrationNumbersForCarsWithColour switch state for returning registration number by passing car colour
const RegistrationNumbersForCarsWithColour = 1

//SlotNumbersForCarsWithColour is switch state for returning slots number by passing car colour
const SlotNumbersForCarsWithColour = 2

//SlotNumberForRegistrationNumber is switch state for returning slots number by passing registration number
const SlotNumberForRegistrationNumber = 3

func find(state int, args []string) string {
	var buffer bytes.Buffer
	switch state {
	case RegistrationNumbersForCarsWithColour:
		_, car, err := service.Find(model.Car{CarColor: args[1]})
		if err != nil {
			return err.Error()
		}
		for i := 0; i < len(car); i++ {
			buffer.WriteString(fmt.Sprint(car[i].RegistrationNumber))
			if i != len(car)-1 {
				buffer.WriteString(", ")
			}
		}

	case SlotNumbersForCarsWithColour:
		slots, _, err := service.Find(model.Car{CarColor: args[1]})
		if err != nil {
			return err.Error()
		}
		for i := 0; i < len(slots); i++ {
			buffer.WriteString(fmt.Sprint(slots[i]))
			if i != len(slots)-1 {
				buffer.WriteString(", ")
			}
		}
	case SlotNumberForRegistrationNumber:
		slots, _, err := service.Find(model.Car{RegistrationNumber: args[1]})
		if err != nil {
			return err.Error()
		}
		for i := 0; i < len(slots); i++ {
			buffer.WriteString(fmt.Sprint(slots[i]))
			if i != len(slots)-1 {
				buffer.WriteString(", ")
			}
		}
	}
	var out string
	if out = buffer.String(); len(out) == 0 {
		out = "Not found"
	}
	return out
}

//Find is used as input output handler for find service call
func Find(state int) string {
	args, err := common.GetArgs(1)
	if err != nil {
		return err.Error()
	}
	return find(state, args)
}
