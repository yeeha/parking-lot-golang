package controller

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestServiceRouting(t *testing.T) {
	// bytes, _ := ioutil.ReadFile("test/assets/results.txt")
	// var actual string
	var err error
	_, err = runWithFile("test/assets/input_file.txt")
	assert.NotEqual(t, nil, err, "test run main using file that not exists")
	_, err = runWithFile("test/assets/file_input.txt")
	assert.Equal(t, nil, err, "test run main using file that exists")
}
