package controller

import (
	"GOJEK/common"
	"bytes"
	"fmt"
	"io/ioutil"
	"os/exec"
	"strings"
)

func execCommand(input string, arg1 string, arg2 string) string {
	var out []byte
	// var err error
	command := fmt.Sprintf("./%s", input)
	// fmt.Printf("CMD: %s %s %s\n", command, arg1, arg2)
	if arg1 != "" && arg2 == "" {
		out, _ = exec.Command(command, arg1).Output()
	} else if arg2 != "" {
		out, _ = exec.Command(command, arg1, arg2).Output()
	} else {
		out, _ = exec.Command(command).Output()
	}

	// if err != nil {
	// 	fmt.Println(err)
	// 	return err
	// }
	// fmt.Print(string(out))
	return string(out)
}

func runWithFile(filename string) (string, error) {

	file, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	lines := strings.Split(string(file), "\n")
	var buffer bytes.Buffer
	var words []string
	for i := 0; i < len(lines); i++ {

		words = strings.Fields(lines[i])

		switch len(words) {
		case 1:
			buffer.WriteString(execCommand(words[0], "", ""))
			// err = execCommand(words[0], "", "")
		case 2:
			// err = execCommand(words[0], words[1], "")
			buffer.WriteString(execCommand(words[0], words[1], ""))
		case 3:
			// err = execCommand(words[0], words[1], words[2]
			buffer.WriteString(execCommand(words[0], words[1], words[2]))
		}
	}
	return buffer.String(), nil
}

func runLineByLine() string {
	var input string
	var arg1 string
	var arg2 string

	fmt.Scanf("%s %s %s", &input, &arg1, &arg2)
	return execCommand(input, arg1, arg2)
}

func routing(args []string) error {
	if len(args) >= 2 {
		var results string
		var err error
		if results, err = runWithFile(args[1]); err != nil {
			return err
		}
		fmt.Print(results)
		return nil
	}
	for true {
		fmt.Print(runLineByLine())
	}
	return nil
}

//ServiceRouting is used to route all request response to main apps
func ServiceRouting() error {
	args, err := common.GetArgs(0)
	if err != nil {
		return err
	}
	return routing(args)
}
