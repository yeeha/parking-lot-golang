package controller

import (
	"GOJEK/common"
	"GOJEK/model"
	"GOJEK/service"
	"fmt"
)

func park(args []string) string {
	var car model.Car
	car.RegistrationNumber = args[1]
	car.CarColor = args[2]
	i, err := service.ParkCar(car)
	if err != nil {
		return err.Error()
	}
	return fmt.Sprintf("Allocated slot number: %d", i+1)
}

//Park is used as input output handler for park service call
func Park() string {
	args, err := common.GetArgs(2)
	if err != nil {
		return err.Error()
	}
	return park(args)
}
