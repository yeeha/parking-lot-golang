package service

import (
	"GOJEK/model"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"sort"
	"strings"
)

//SaveParkingLotData is
func SaveParkingLotData(parkingLot model.ParkingLot) error {
	os.Remove(model.Datasource)
	json, err := json.Marshal(parkingLot)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(model.Datasource, json, 0644)
	return err
}

//LoadParkingLotData is
func LoadParkingLotData(parkingLot *model.ParkingLot) error {
	file, err := ioutil.ReadFile(model.Datasource)
	if err != nil {
		return err
	}
	err = json.Unmarshal(file, parkingLot)
	return err
}

//CreateParkingLot is
func CreateParkingLot(total int) {
	parkingLot := model.ParkingLot{Total: total}
	for i := 0; i < total; i++ {
		parkingLot.Slots = append(parkingLot.Slots, i)
		parkingLot.Cars = append(parkingLot.Cars, model.Car{})
	}
	SaveParkingLotData(parkingLot)
}

//ParkCar is
func ParkCar(car model.Car) (int, error) {
	var parkingLot model.ParkingLot
	LoadParkingLotData(&parkingLot)
	if len(parkingLot.Slots) <= 0 {
		return 0, errors.New("Sorry, parking lot is full")
	}
	i := parkingLot.Slots[0]
	parkingLot.Cars[parkingLot.Slots[0]] = car
	parkingLot.Slots = append(parkingLot.Slots[:0], parkingLot.Slots[1:]...)
	SaveParkingLotData(parkingLot)
	return i, nil
}

//Leave is
func Leave(i int) error {
	var parkingLot model.ParkingLot
	LoadParkingLotData(&parkingLot)
	if len(parkingLot.Slots) >= parkingLot.Total {
		return errors.New("No car on parking lot")
	}
	parkingLot.Cars[i-1] = model.Car{}
	parkingLot.Slots = append(parkingLot.Slots, i-1)
	sort.Ints(parkingLot.Slots)
	SaveParkingLotData(parkingLot)
	return nil
}

//Status is
func Status() string {
	var parkingLot model.ParkingLot
	LoadParkingLotData(&parkingLot)

	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("%-3s\t\t%-30s\t\t%-10s\n", "No.", "Registration Number", "colour"))

	for i := 0; i < len(parkingLot.Cars); i++ {
		if reflect.DeepEqual(model.Car{}, parkingLot.Cars[i]) == false {
			buffer.WriteString(fmt.Sprintf("%-3d\t\t%-30s\t\t%-10s\n", i+1, parkingLot.Cars[i].RegistrationNumber, parkingLot.Cars[i].CarColor))
		}
	}
	return strings.TrimSpace(buffer.String())
}

//Find is
func Find(car model.Car) ([]int, []model.Car, error) {
	if reflect.DeepEqual(car, model.Car{}) {
		return nil, nil, errors.New("Search Criteria Empty")
	}

	var parkingLot model.ParkingLot
	var out []model.Car
	var outI []int
	LoadParkingLotData(&parkingLot)
	i := 0
	for i < len(parkingLot.Cars) {
		if (car.CarColor == parkingLot.Cars[i].CarColor && car.RegistrationNumber == "") || (car.CarColor == "" && car.RegistrationNumber == parkingLot.Cars[i].RegistrationNumber) {
			outI = append(outI, i+1)
			out = append(out, parkingLot.Cars[i])
		}
		i++
	}
	return outI, out, nil
}
