package service

import (
	"GOJEK/model"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSaveParkingLotData(t *testing.T) {
	car := model.Car{}
	cars := []model.Car{}
	car.CarColor = "red"
	car.RegistrationNumber = "KA-01-HH-1234"
	cars = append(cars, car)
	parkingLot := model.ParkingLot{Cars: cars}

	assert.Equal(t, nil, SaveParkingLotData(parkingLot), "unit test save data to datasource")
}

func TestLoadParkingLoadData(t *testing.T) {
	var parkingLot model.ParkingLot

	assert.Equal(t, nil, LoadParkingLotData(&parkingLot), "unit test load data")
	os.Remove(model.Datasource)
	assert.NotEqual(t, nil, LoadParkingLotData(&parkingLot), "unit test load data when datasource is missing")
}

func TestCreateParkingLot(t *testing.T) {
	n := 10
	var parkingLot model.ParkingLot
	CreateParkingLot(n)
	assert.Equal(t, nil, LoadParkingLotData(&parkingLot), "unit test load data after create parking load")
	assert.Equal(t, n, parkingLot.Total, "unit test get car capacity on parking lot")
	assert.Equal(t, n, len(parkingLot.Cars), "unit test car parking area should be created on parking lot")
	os.Remove(model.Datasource)
}

func TestParkCar(t *testing.T) {
	car := model.Car{CarColor: "red", RegistrationNumber: "KA-01-HH-1234"}
	i, err := ParkCar(car)
	assert.NotEqual(t, nil, err, "unit test park a car when no parking lot created")
	assert.Equal(t, 0, i, "unit test no slots are given when car arent parked yet")

	n := 2
	CreateParkingLot(n)

	ParkCar(car)
	var parkingLot model.ParkingLot
	LoadParkingLotData(&parkingLot)
	assert.Equal(t, n-1, len(parkingLot.Slots), "unit test park car after parking lot created")
	os.Remove(model.Datasource)
}

func TestLeave(t *testing.T) {
	n := 2
	leave := 1
	car := model.Car{CarColor: "red", RegistrationNumber: "KA-01-HH-1234"}
	CreateParkingLot(n)
	ParkCar(car)
	// ParkCar(car)
	Leave(leave)
	var parkingLot model.ParkingLot
	LoadParkingLotData(&parkingLot)
	assert.Equal(t, parkingLot.Total, len(parkingLot.Slots), "unit test parking lot total space should be equal to slots when no car are parked yet (park then leave)")
	assert.Equal(t, model.Car{}, parkingLot.Cars[leave], "unit test after car leave, space should be empty")
	assert.NotEqual(t, nil, Leave(1), "unit test when no car park on parking lot, but leave command is called")
	os.Remove(model.Datasource)
}

func TestFind(t *testing.T) {
	CreateParkingLot(4)
	ParkCar(model.Car{RegistrationNumber: "KA-01-HH-1234", CarColor: "White"})
	ParkCar(model.Car{RegistrationNumber: "KA-01-HH-9999", CarColor: "White"})
	ParkCar(model.Car{RegistrationNumber: "KA-01-BB-0001", CarColor: "Black"})
	_, _, err := Find(model.Car{})
	assert.NotEqual(t, nil, err, "unit test find car without criteria")
	i, car, _ := Find(model.Car{RegistrationNumber: "KA-01-HH-9999"})
	assert.Equal(t, 2, i[0], "unit test find parking slot by given registration number ")
	assert.Equal(t, "White", car[0].CarColor, "unit test find car color by given registration number")
	i, car, _ = Find(model.Car{CarColor: "White"})
	var tempI []int
	tempI = append(tempI, 1)
	tempI = append(tempI, 2)
	assert.Equal(t, tempI, i, "unit test find cars slots by its color")
	var temp []model.Car
	temp = append(temp, model.Car{RegistrationNumber: "KA-01-HH-1234", CarColor: "White"})
	temp = append(temp, model.Car{RegistrationNumber: "KA-01-HH-9999", CarColor: "White"})
	assert.Equal(t, temp, car, "unit test find cars by its color")
	// assert.Equal(t, "White", car.CarColor, "Test Find")
}

func removeAll(input string) string {
	temp := strings.Replace(input, "\t", "", -1)
	temp2 := strings.Replace(temp, "\n", "", -1)
	out := strings.Replace(temp2, " ", "", -1)
	return out
}

func TestStatus(t *testing.T) {
	bytes, _ := ioutil.ReadFile("test/assets/results.txt")

	assert.Equal(t, removeAll(string(bytes)), removeAll(Status()), "unit test print current status of parking lot by remove \n \r and white space")
	os.Remove(model.Datasource)
}
