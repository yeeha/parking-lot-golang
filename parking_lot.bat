go get github.com/stretchr/testify/assert
go test .\controller/...
go test .\service/...
go build create_parking_lot.go
go build leave.go
go build park.go
go build registration_numbers_for_cars_with_colour.go
go build slot_number_for_registration_number.go
go build slot_numbers_for_cars_with_colour.go
go build status.go
go build main.go
main.exe %1
