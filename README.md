# Created For Both Developers and Users

>**MicroServices** Age it's here. The apps build based on MicroServices concept.
All function splits into several services (create, park, leave, find, status) all micro. It's tidy, easy to maintenance and enhance. Core function are saved into one package and possible to moved as standalone library (but now allowed by the rules).
Its Also used **json as filebase datasource**

# Parking Lot Application (CLI)

It's here now, parking lot application that enable to maintain business process on parking lot space with capacity management and status monitoring.
It's GO based, so it's build in concurrency help much reduce processing time.

CHEERS....

## Getting Started

GOLANG NEEDED, PLEASE ACCESSS [HERE](https://golang.org/doc/install?download=go1.5.windows-amd64.msi2) TO INSTALL GOLANG:

~~then to get this project, just simply called:~~ **haven't vendorized by rules**
```
GO GET bitbucket.org/yodyhariadi/parkinglot/
```

### Prerequisites

GOLANG

```
(https://golang.org/doc/install?download=go1.5.windows-amd64.msi2)
```

Test Tools:
```
GO GET github.com/stretchr/testify/assert
```

**ATTENTION**: user used to run the program need to be able to create files (inside **GOJEK** folder). the program will create datasource called as **parkinglot.json**. Project **havent vendorized** yet (not allowed by rules), please init git project inside folder GOJEK (case sensitive). If **./parking_lot** not work, please make sure it is execute-able (chmod 777).

### Installing

~~By Go Get~~ **haven't vendorized by rules**

```
GO GET bitbucket.org/yodyhariadi/parkinglot/
```

By GIT

```
[Private Repository](https://yodyhariadi@bitbucket.org/yodyhariadi/parkinglot.git)
```

## Running the tests

Unit test include on build.sh

### Break down into end to end tests

#####Controller Test Case:
* Test input output handler
* End to End Test (use given example as test data)

#####Service Test Case:
* Test functionality (unit test)

### And coding style tests

* It's TDD, test case first then code follow up :)
* Function to return error (nil on OK state)

```go
func TestIsEven(t *testing.T){
	//use lib (Read Prerequisites)
	assert.NotEqual(t, nil, isEven(3),"Test given odd number")
	assert.Equal(t, nil, isEven(2),"Test given even number")
}

func isEven(input int) error {
	//Close Guard
	if input%2 != 0 {
		return errors.New("number is not Even")
	}
	return nil
}
```

## Deployment

Windows:
```
main.exe
```

Linux:
```
./main
```

## Built With (include RUNNING)

Windows:
```
parking_lot.bat
```

Linux:
```
parking_lot.sh
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

My first day
V 1.0.0

## Authors

* **Yoedi Hariadi Kurniawan** - *Passionate Software Engineer*

## License



## Acknowledgments
