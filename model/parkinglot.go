package model

//ParkingLot is
type ParkingLot struct {
	Slots []int
	Cars  []Car
	Total int
}
