package common

import (
	"fmt"
	"os"
)

//GetArgs is function to get cli arguments
func GetArgs(total int) ([]string, error) {
	if len(os.Args) < total+1 {
		return nil, fmt.Errorf("At least %d argument needed", total)
	}
	return os.Args, nil
}
